import QtQuick 2.9
import QtQuick.Window 2.2
import QtStudio3D 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Vehicle Controller")

    Studio3D{
        id : studio
        anchors.fill: parent

        Presentation {
            id: cluster
            source: "qrc:/presentation/3d_example.uia"

            ComponentElement{
                id: vehicle
                elementPath: "Scene.Layer.LANCEREVOX"
                property alias isRotating: rotating.running
                NumberAnimation on time{
                    id: rotating
                    running: false
                    from: 0.0
                    to: 5.0
                    duration: 2500
                }
            }

            ComponentElement{
                id: doorL
                elementPath: "Scene.Layer.LANCEREVOX.LANCEREVOX.DoorLGroup"
                property bool isOpen: false
                NumberAnimation on time{
                    id: openDoor
                    running: false
                    from: 0.0
                    to: 1.0
                    duration: 300
                }
                NumberAnimation on time{
                    id: closeDoor
                    running: false
                    from: 1.0
                    to: 0.0
                    duration: 300
                }
            }

            ComponentElement{
                id: headlampLight
                elementPath: "Scene.Layer.LANCEREVOX.LANCEREVOX.Light"
                property bool isOn: false
                NumberAnimation on time{
                    id: onLight
                    running: false
                    from: 0.0
                    to: 1.0
                    duration: 200
                }
                NumberAnimation on time{
                    id: offLight
                    running: false
                    from: 1.0
                    to: 0.0
                    duration: 200
                }
            }

            ComponentElement{
                id: tireFL
                elementPath: "Scene.Layer.LANCEREVOX.LANCEREVOX.TireFL"
                NumberAnimation on time{
                    id: driveFL
                    loops: Animation.Infinite
                    running: false
                    from: 0.0
                    to: 5.0
                    duration: 200
                }
            }

            ComponentElement{
                id: tireFR
                elementPath: "Scene.Layer.LANCEREVOX.LANCEREVOX.TireFR"
                NumberAnimation on time{
                    id: driveFR
                    loops: Animation.Infinite
                    running: false
                    from: 0.0
                    to: 5.0
                    duration: 200
                }
            }

            ComponentElement{
                id: tireRL
                elementPath: "Scene.Layer.LANCEREVOX.LANCEREVOX.TireRL"
                NumberAnimation on time{
                    id: driveRL
                    loops: Animation.Infinite
                    running: false
                    from: 0.0
                    to: 5.0
                    duration: 200
                }
            }

            ComponentElement{
                id: tireRR
                elementPath: "Scene.Layer.LANCEREVOX.LANCEREVOX.TireRR"
                NumberAnimation on time{
                    id: driveRR
                    loops: Animation.Infinite
                    running: false
                    from: 0.0
                    to: 5.0
                    duration: 200
                }
            }
        }
    }

    Controller{
        id: topController
    }
}
