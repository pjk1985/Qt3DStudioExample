import QtQuick 2.9
import QtStudio3D 1.0

SceneElement {
    property real time: 0.0

    onTimeChanged: {
        goToTime(time)
    }
}
