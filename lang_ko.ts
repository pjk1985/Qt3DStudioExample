<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>Controller</name>
    <message>
        <location filename="Controller.qml" line="18"/>
        <source>Rotate</source>
        <translation>회전</translation>
    </message>
    <message>
        <location filename="Controller.qml" line="30"/>
        <source>Door</source>
        <translation>도어</translation>
    </message>
    <message>
        <location filename="Controller.qml" line="51"/>
        <source>Headlamp</source>
        <translation>전조등</translation>
    </message>
    <message>
        <location filename="Controller.qml" line="71"/>
        <source>Drive</source>
        <translation>주행</translation>
    </message>
</context>
<context>
    <name>MenuBar</name>
    <message>
        <source>Rotate</source>
        <translation type="vanished">회전</translation>
    </message>
    <message>
        <source>Door</source>
        <translation type="vanished">도어</translation>
    </message>
    <message>
        <source>Headlamp</source>
        <translation type="vanished">전조등</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation type="vanished">주행</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Hello World</source>
        <translation type="vanished">안녕</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">램프</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation type="vanished">주행</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation type="vanished">회전</translation>
    </message>
    <message>
        <location filename="main.qml" line="9"/>
        <source>Vehicle Controller</source>
        <translation>차량 제어기</translation>
    </message>
</context>
</TS>
