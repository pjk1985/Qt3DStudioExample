import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

Rectangle{
    id: controller
    width: parent.width
    height: 40
    color: "white"
    anchors.top: parent.top

    RowLayout {
        anchors.fill: parent
        Layout.alignment: Qt.AlignHCenter

        Button{
            id: buttonRotate
            text: qsTr("Rotate")
            anchors.verticalCenter: parent.verticalCenter
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                if(vehicle.isRotating)
                    rotating.restart()
                else
                    rotating.start()
            }
        }
        Button{
            id: buttonDoorL
            text: qsTr("Door")
            anchors.verticalCenter: parent.verticalCenter
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                if(doorL.isOpen)
                {
                    doorL.isOpen = false
                    openDoor.running = false
                    closeDoor.running = true
                }
                else
                {
                    doorL.isOpen = true
                    closeDoor.running = false
                    openDoor.running = true

                }
            }
        }
        Button{
            id: buttonLamp
            text: qsTr("Headlamp")
            anchors.verticalCenter: parent.verticalCenter
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                if(headlampLight.isOn)
                {
                    headlampLight.isOn = false
                    onLight.running = false
                    offLight.running = true
                }
                else
                {
                    headlampLight.isOn = true
                    onLight.running = true
                    offLight.running = false
                }
            }
        }
        Button{
            id: buttonDrive
            text: qsTr("Drive")
            anchors.verticalCenter: parent.verticalCenter
            Layout.alignment: Qt.AlignHCenter
            onClicked:{
                if(driveFL.running)
                {
                    driveFL.stop()
                    driveFR.stop()
                    driveRL.stop()
                    driveRR.stop()
                }
                else
                {
                    driveFL.start()
                    driveFR.start()
                    driveRL.start()
                    driveRR.start()
                }
            }
        }
    }
}
